Simple conversion from the vtt format (as dumped from youtube) into (a JSON format with) words + timing.

Usage:

	./vtt2words.py --format json something.en.vtt > output.json

Example output:

```json
{
  "transcription": "the contemporary condition as soon as you think about ...", 
  "words": [
    {
      "x": "the", 
      "t": 0.89
    }, 
    {
      "x": "contemporary", 
      "t": 1.89
    }, 
    {
      "x": "condition", 
      "t": 2.429
    }, 
    {
      "x": "as", 
      "t": 3.12
    }, 
    {
      "x": "soon", 
      "t": 3.389
    }, 
    {
      "x": "as", 
      "t": 3.659
    }, 
    {
      "x": "you", 
      "t": 3.84
    }, 
    {
      "x": "think", 
      "t": 4.279
    }, 
    {
      "x": "about", 
      "t": 5.279
    },
    ...
}
```

