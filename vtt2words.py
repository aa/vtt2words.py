#!/usr/bin/env python 
from __future__ import print_function
import argparse
import re, sys


def tc (c):
    m = re.search(r"(\d\d):(\d\d):(\d\d\.\d\d\d)", c)
    h, m, s = int(m.group(1)), int(m.group(2)), float(m.group(3))
    return h*3600+m*60+s

def vtt2words (f):
    start = None
    words = []
    for line in args.input:
        # print (line, file=sys.stderr)
        m = re.search("^(\d\d:\d\d:\d\d\.\d\d\d) --> (\d\d:\d\d:\d\d\.\d\d\d)", line)
        if m:
            start, end = tc(m.group(1)), tc(m.group(2))
            # print ("--> {0} {1}".format(start,end), file=sys.stderr)
        else:
            for token in re.split(r"(<.+?>)", line):
                if token.startswith("<") and token.endswith(">"):
                    m = re.search(r"<(\d\d:\d\d:\d\d\.\d\d\d)>", token)
                    if m:
                        start = tc(m.group(1))
                        # print ("<> {0}".format(start), file=sys.stderr)
                elif start != None:
                    token = token.strip()
                    if token:
                        words.append((start, token))
    if end > start:
        words.append((end, ""))
    return words

if __name__ == "__main__":
    ap = argparse.ArgumentParser("")
    ap.add_argument("input", nargs="?", type=argparse.FileType('r'), default=sys.stdin)
    ap.add_argument("--format", default=None, help="*text, json")
    args = ap.parse_args()

    words = vtt2words(args.input)
    if args.format == "json":
        import json
        text = " ".join([x for t, x in words]).strip()
        words = [{'x': x, 't': t} for t, x in words]
        print (json.dumps({'transcription': text, 'words': words}, indent=2))
    else:
        for t, w in words:
            print (t, w)
